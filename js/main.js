CanvasRenderingContext2D.prototype.fillTextCircle = function(text, x, y, radius, startRotation) {
    var numRadsPerLetter = 2 * Math.PI / text.length;
    this.save();
    this.translate(x, y);
    this.rotate(startRotation);
    for (var i = 0; i < text.length; i++) {
        this.save();
        this.rotate(i * numRadsPerLetter);
        this.fillText(text[i], 0, -radius);
        this.restore();
    }
    this.restore();
}
require.config({
    paths: {
        echarts: './js'
    }
});

CenterY = 180;
defRound = 48;
$(function() {
    var hammertime = new Hammer(document.getElementsByClassName('b-pair')[0]);
	hammertime.on('swipe', function(ev) {
		changePair();
	});
	var options = {
		preventDefault: true
	};
	var hammerzoom = new Hammer(document.getElementById('main'),options);
	hammerzoom.get('pinch').set({ enable: true });
	hammerzoom.get('pan').set({ enable: true, direction: Hammer.DIRECTION_VERTICAL });
	
	hammerzoom.on('pan', function(ev) {
	//	alert(ev.deltaY);
	   window.scrollTo(0,window.scrollY-ev.deltaY);
	});
	
	hammerzoom.on('pinchin', function(ev) {
	    if (defRound<58)
			defRound += 0.5;
		setTriangle(defRound);
	});
	hammerzoom.on('pinchout', function(ev) {
		if (defRound>48.7)
			defRound -= 0.5;
		setTriangle(defRound);
	});
	
	$( ".pair_arrow" ).on( "click", function() {
	  changePair();
	});
})
function changePair(){
	if ($('.pair_name').attr('data-value')=='usd'){
		$('.pair_name').attr('data-value','jpn')
		$('.pair_name').html('EUR/JPN');
	}else{
		$('.pair_name').attr('data-value','usd')
		$('.pair_name').html('EUR/USD');
	}
}
function setTriangle(s){
	var r = 140*koef;
	var width = Math.ceil(zr.getWidth());
	triangle.style.pointList = [[width/2 + (r+5) * Math.sin(6*(s+0.5)*3.14159/180), CenterY - (r+5) * Math.cos(6*(s+0.5)*3.14159/180)],
									[width/2 + r * Math.sin(6*(s+1)*3.14159/180), CenterY - r * Math.cos(6*(s+1)*3.14159/180)], 
									[width/2 + r * Math.sin(6*s*3.14159/180), CenterY - r * Math.cos(6*s*3.14159/180)]];
	zr.modShape(triangle);
	timeSelectShapes.day.style.color="#8292aa";
	timeSelectShapes.week.style.color="#8292aa";
	timeSelectShapes.month.style.color="#8292aa";
	timeSelectShapes.year.style.color="#8292aa";
	if (s<50.6) timeSelectShapes.day.style.color="#99ddff"; else
		if (s<53.2) timeSelectShapes.week.style.color="#99ddff"; else
			if (s<56.5) timeSelectShapes.month.style.color="#99ddff"; else
				timeSelectShapes.year.style.color="#99ddff" 
	
	zr.modShape(timeSelectShapes.day);
	zr.modShape(timeSelectShapes.week);
	zr.modShape(timeSelectShapes.month);
	zr.modShape(timeSelectShapes.year);
}

function addCustomElements() {
    var width = Math.ceil(zr.getWidth());
    var height = Math.ceil(zr.getHeight());
    var RingShape = require('zrender/shape/Ring');
    var ImageShape = require('zrender/shape/Image');
    var TextShape = require('zrender/shape/Text');
	var CircleShape = require('zrender/shape/Circle');	
	var PolygonShape = require('zrender/shape/Polygon');
    zr.addShape(new RingShape({
        style: {
            x: width / 2,
            y: CenterY,
            r: roundRadius+3,
            r0: roundRadius,
            color: '#8292aa',
        },
        hoverable: false,
    }));
    var image = new ImageShape({
        style: {
            image: './img/clock-tick.png',
            x: width / 2 - 130*koef,
            y: CenterY - 130*koef,
            width: 260*koef,
            height: 260*koef,
            //height: 100,
        },
        hoverable: false,
    });

    zr.addShape(image);
    var Base = require('zrender/shape/Base');
	//round text
    function RoundText(options) {
        Base.call(this, options);
    }
    RoundText.prototype = {
        type: 'RoundText',
        brush: function(ctx, isHighlight) {
            var style = this.style;
            var text = style.text || "";
            var color = style.color || "black";
			var radius = style.radius || (120*koef);
            var startRotation = style.startRotation || 0.05;
		    var font = style.font || "12px Arial";
            ctx.font = font;
			
            ctx.fillStyle = color;
            ctx.fillTextCircle(text, width / 2, CenterY, radius, startRotation);
            ctx.save();
            return;
        },
        drift: function(dx, dy) {
            this.style.x += dx;
            this.style.y += dy;
        },
        isCover: function(x, y) {
            return false;
        }
    }
    require('zrender/tool/util').inherits(RoundText, Base);
    zr.addShape(new RoundText({
        style: {
            text: greenText,
            color: "#4abe87",
        },
    }));
    zr.addShape(new RoundText({
        style: {
            text: redText,
            color: "#c4355e",
        },
    }));
	timeSelectShapes = {};
	timeSelectShapes.day  = new RoundText({
        style: {
            text: "DAY                                                                                                   ",
            color: "#99ddff",
			radius: 150*koef,
			font: "12px arial",
			startRotation: 5.1,
        },
    })
	zr.addShape(timeSelectShapes.day);
	timeSelectShapes.week = new RoundText({
        style: {
            text: "WEEK                                                                                                  ",
            color: "#8292aa",
			radius: 150*koef,
			startRotation: 5.35,
			font: "12px arial",
        },
    })
	zr.addShape(timeSelectShapes.week);
	timeSelectShapes.month = new RoundText({
        style: {
            text: "MONTH                                                                                                 ",
            color: "#8292aa",
			radius: 150*koef,
		    startRotation: 5.65,
			font: "12px arial",
        },
    })
	zr.addShape(timeSelectShapes.month);
	timeSelectShapes.year = new RoundText({
        style: {
            text: "YEAR                                                                                                  ",
            color: "#8292aa",
			radius: 150*koef,
			startRotation: 6,
			font: "12px arial",
        },
    })
	zr.addShape(timeSelectShapes.year);
	triangle = new PolygonShape({
					style : {
						pointList : [[310, 120], [360, 120], [348, 230]],
						brushType : 'both',
						color : "#99ddff",
						lineWidth : 0,	
					},
					draggable:false,
					hoverable: false,
				});
	setTriangle(49);
	zr.addShape(triangle);
	var LineShape = require('zrender/shape/Line');
	clock =[];
	zr.addShape(new CircleShape({
				style : {
					x : width / 2,
					y : CenterY,
					r : 12*koef,
					brushType : 'both',
					color : '#ffffff',        
				},
				hoverable : false,   
				draggable : false,   
	
				
			}));
	clock.hoursLine = new LineShape({
		style : {
			xStart : width / 2,//width/2 + 120 * Math.sin(6*0*3.14159/180),
			yStart : CenterY,//250 - 120 * Math.cos(6*0*3.14159/180),
			lineJoin : 'round',
			xEnd : width / 2,
			yEnd : CenterY,
			strokeColor : "#ffffff",
			shadowBlur: 3,
			shadowOffsetX: 0,
			shadowOffsetY: 0,
			shadowColor: '#ffffff',
			lineWidth : 3,
			lineType : 'solid',   
		},
		hoverable: false,
	});
	zr.addShape(clock.hoursLine);
	

	clock.minutesLine = new LineShape({
		style : {
			xStart : width / 2,//width/2 + 120 * Math.sin(6*0*3.14159/180),
			yStart : CenterY,//250 - 120 * Math.cos(6*0*3.14159/180),
			lineJoin : 'round',
			xEnd : width / 2,
			yEnd : CenterY,
			strokeColor : "#ffffff",
			shadowBlur: 3,
			shadowOffsetX: 0,
			shadowOffsetY: 0,
			shadowColor: '#ffffff',
			lineWidth : 3,
			lineType : 'solid',  
		},
		hoverable: false,
	});
	zr.addShape(clock.minutesLine);
	zr.addShape(new CircleShape({
				style : {
					x : width / 2,
					y : CenterY,
					r : 6*koef,
					brushType : 'both',
					color : '#99ddff', 
				},
				hoverable : false,   
				draggable : false,   
	
				
			}));
	clock.secondLine = new LineShape({
		style : {
			xStart : width / 2,//width/2 + 120 * Math.sin(6*0*3.14159/180),
			yStart : CenterY,//250 - 120 * Math.cos(6*0*3.14159/180),
			lineJoin : 'round',
			xEnd : width / 2,
			yEnd : CenterY,
			strokeColor : "#99ddff",
			lineWidth : 2,
			shadowBlur: 3,
			shadowOffsetX: 0,
			shadowOffsetY: 0,
			shadowColor: '#99ddff',
			lineType : 'solid',  
		},
		hoverable: false,
	});
	zr.addShape(clock.secondLine);
	
	
	intervalID = setInterval(function(){
		var d = new Date();
		var h = d.getHours();
		if (h>12){
			h -= 12;
		}
		clock.hoursLine.style.xStart = width/2 + 70*koef * Math.sin(30*h*3.14159/180);
		clock.hoursLine.style.yStart = CenterY - 70*koef * Math.cos(30*h*3.14159/180);
		zr.modShape(clock.hoursLine);
	
		var m = d.getMinutes();
		clock.minutesLine.style.xStart = width/2 + 90*koef * Math.sin(6*m*3.14159/180);
		clock.minutesLine.style.yStart = CenterY - 90*koef * Math.cos(6*m*3.14159/180);
		zr.modShape(clock.minutesLine);
		
		var s = d.getSeconds();
		if (s>=30)
			var as = s-30;
		else
			var as = s+30;
		clock.secondLine.style.xStart = width/2 + 110*koef * Math.sin(6*s*3.14159/180);
		clock.secondLine.style.yStart = CenterY - 110*koef * Math.cos(6*s*3.14159/180);
		
		clock.secondLine.style.xEnd = width/2 + 20*koef * Math.sin(6*as*3.14159/180);
		clock.secondLine.style.yEnd = CenterY - 20*koef * Math.cos(6*as*3.14159/180);
		
		zr.modShape(clock.secondLine);
		
		},100);
}
require(
    [
        'echarts',
        'echarts/chart/pie'
    ],
    function(ec, theme) {
		updateKoef()
		$('#main').css('height',300*koef);
        myChart = ec.init(document.getElementById('main'), theme);

        myChart.setOption(getOption());
        zr = myChart.getZrender();

        window.onresize = function() {
            zr.clear();
			
			myChart.setOption(getOption());
			$('#main').css('height',300*koef);
            myChart.resize();
			clearInterval(intervalID);
            addCustomElements();
		
        }
        addCustomElements()



    }
);