Array.prototype.max = function() {
  return Math.max.apply(null, this);
};
Array.prototype.min = function() {
  return Math.min.apply(null, this);
};
function getSerie(arrName,absoluteMin,absoluteMax,colorUp,colorDown){
	var chartMax = [];
	for (var i = 0; i < 24; i++) {
		var item = data[i][arrName];
		if (data[i].Close<data[i].Open){
			var labelColor = '#c4355e';
			var color = colorDown;
		}
		else{
			var labelColor = '#49be87';
			var color = colorUp;
		}
		var showLabelLine = false;
		var showText = "";
		var borderColor = '#65a477';
		var borderWidth =0.25;
		if (arrName=='Min'){
			borderColor = '#344256';
			borderWidth = 1;
		}
		if (arrName=='Max')
			showLabelLine = false;
		if (arrName=='Max'){
			
			showText = data[i].Close;
		}
			
		chartMax.push({value: (item-absoluteMin)*absoluteMax/(absoluteMax-absoluteMin)*100,
					name: showText,
					itemStyle : {
						normal : {
							color:color,
							borderColor: borderColor,
							borderWidth: borderWidth,
							label : {
								show : false,
								position:'outher',
								textStyle :{
									color: labelColor
								}
							},
							labelLine : {
								show : showLabelLine,

							}
						},
						emphasis : {
							color:'transparent'
						}
					},
					});
	};
	return chartMax;
}
function updateKoef(){
	var width = window.innerWidth;
	if (window.innerWidth > 700)
		width = 700;
	roundRadius = width * 0.8 / 2;
	koef = roundRadius / 130;
	CenterY = 160*koef;
}
function getOption(){
	updateKoef();
	//CenterY *= koef;
	var minCharts   = [];
	var maxCharts   = [];
	var openCharts  = [];
	var closeCharts = [];
	redText = "";
	greenText = "";
	data.forEach(function(item, i, arr) {
	  minCharts.push(item.Min);
	  
	  if (i<24){
		if (item.Close<item.Open){
			redText += (item.Close).toFixed(2)+'   ';
			greenText += '       ';
		}else{
			redText += '       ';
			greenText += (item.Close).toFixed(2)+'   ';
		}
	  }
	 
		
	  maxCharts.push(item.Max);
	  openCharts.push(item.Open);
	  closeCharts.push(item.Close);
	});
	var absoluteMax = maxCharts.max();
	var absoluteMin = minCharts.min();
	var delta = absoluteMax - absoluteMin;
	var absoluteMin = absoluteMin - delta*0.3;

	var option = {
		animation: false,
		backgroundColor: '#344256',
		color: ['#49be87'],
		series : [
			{
				z: 0,
				name:'max',
				clickable: false,
				label:	{show: false},
				type:'pie',
				radius : [0, 115*koef],
				center : ['50%', CenterY],
				roseType : 'area',
				width: '8.33%',       // for funnel
				max: 100,            // for funnel
				data: getSerie('Max',absoluteMin,absoluteMax,'#49A660',"#328c45")
			},
			{
				z: 0,
				name:'Open',
				label:	{show: false},
				clickable: false,
				type:'pie',
				radius : [0, 90*koef],
				center : ['50%', CenterY],
				roseType : 'area',
				width: '8.33%',       // for funnel
				max: 100,            // for funnel
				data: getSerie('Close',absoluteMin,absoluteMax,'#78c48d','#267936')
			},
			{
				z: 0,
				name:'Close',
				label:	{show: false},
				clickable: false,
				type:'pie',
				radius : [0, 60*koef],
				center : ['50%', CenterY],
				roseType : 'area',
				width: '8.33%',       // for funnel
				max: 100,            // for funnel
				data: getSerie('Open',absoluteMin,absoluteMax,'#49A660','#328c45')
				
			},
			{
				z: 0,
				name:'Min',
				label:	{show: false},
				clickable: false,
				
				type:'pie',
				radius : [0, 30*koef],
				center : ['50%', CenterY],
				roseType : 'area',
				width: '8.33%',       // for funnel
				max: 100,            // for funnel
				data: getSerie('Min',absoluteMin,absoluteMax,'#344256','#344256')
			}
		],	
	};
	return option;
}